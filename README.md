# da_mcs_openmct



## Place for preliminary prototyping of MCS components in openMCT

## Goals
- Examination of feasibility and usefulness of openMCT for MCS purposes
- Experimentation and extension with basic MCS components based off of viz-frame openMCT modification.

## Code Layout
![System Layout](openMCT_system_diagram.drawio.png)
